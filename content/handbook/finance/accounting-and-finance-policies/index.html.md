---
layout: markdown_page
title: "Accounting and Finance Policies"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## Accounting and Finance Policies

This page contains GitLab's accounting and finance policies.

### Assets
- [Capital Assets](/handbook/finance/capital-assets-policy/)
- [Investment Policy](/handbook/finance/investment-policy/)
- [Prepaid Expenses](/handbook/finance/prepaid-expense-policy/)

### Liabilities
- [Accrued Liabilities](/handbook/finance/accrued-liabilities-policy/)
- [Procure-to-Pay](/handbook/finance/procure-to-pay/)
 
### Equity
- [Foreign Currency Translation Policy](/handbook/finance/foreign-currency-translation-policy/)
