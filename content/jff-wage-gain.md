---
title: "JFF $1B Wage Gain Challenge"
draft: false
---
## Name of lead organization or company
Basa Casa, Inc.

##Organization or company mission
Teach the world to use Four-Component Instructional Design.

## Names and websites of additional partner organizations
Dr. Jeroen J.G. van Merrienboer. I reached out to him earlier this year and he supported the idea.
Companies that agree to implement the model for the $1B challenge will be added to /wage-gain.

## Organization or company website
http://www.basa.casa
 
## Contact first name
Brian
## Contact last name
MacKinney
## Provide a concise description of your idea(s) (three to five sentences).
I believe that more effective instruction, applied broadly, will improve outcomes for students who receive it, resulting in more likely wage growth.

Businesses and schools will commit to trying the model, calculating estimated wage gains and numbers of trainees per complex skill (or job) trained for

## How did you hear about the $1 Billion Wage Gain Challenge?
Social media
##Describe the core features of your idea(s) and how it works, using plain language and no jargon. Specifically, how will your idea(s) achieve the goal of raising the annual wages of 100,000 US low- and/or middle-income workers by $10,000 each, for a total of $1B in wage gains by the end of 2021?
The MVP is a project template for creating instructional designs that implement the Four-Component Instructional Design Model, delivered on gitlab.com and github.com. It works the way any project on GitLab or GitHub works. People in an organization who train others to perform complex skills come together in their groups around the project and design instruction using the model. Trainees and students receive the instruction, and learn more, more permanently than they otherwise would, given a normal (fragmented or compartmentalized) course of study. 

## Describe the population(s) of US workers targeted in your idea(s).
All workers are targeted.

## Describe the basis of evidence for why you think this idea(s) will work. Include a short description of how your idea(s) has been informed by users/workers, as relevant.
Four-Component Instructional Design was created to solve this problem most eloquently, by Jeroen J.G. van Merrienboer, while on sabbatical and visiting John Sweller in Sydney, Australia.

## What makes your idea(s) interesting/innovative and capable of moving us beyond the status quo?
My idea is based on an assumption that everyone is capable of growth

## For existing ideas, describe your medium-term growth and sustainability plan (sustainability of both the wages and the idea(s). For pilot ideas, describe your strategy for scaling beyond initial pilot.
To scale beyond the initial pilot, we will design for scalability. I think my initial inclination, to use GitLab and GitHub to distribute a free template in an open-source community. Our

## What are the key barriers threatening the actualization of your idea(s)? How do you plan to address those barriers/mitigate those risks?
1. People don't know they need this yet, because they don't know it exists. This can be mitigated by a combination of making it free to learn the model, promoting the idea. 
1. This model is complex. Designs and implementations can be very complex. Until a sufficient base of working example designs are available to learn from, organizations will experience more trial and error than might be efficient. This can be mitigated by encouraging open-source, continuous delivery as the model for basacasas, so that a community of practice can learn from each other. 

## If you have a team in place, who is on it? Describe the capacity of this team to implement this idea(s)
I don't have a team in place. It's just me trying to leverage GitLab to make a learning solution for myself and my child.

## If you have partners, list and describe them, including their roles and capacity for implementing the idea(s)
Partners List with description and capacity

## Who are the dream partners you would like to recruit to work with you to implement this idea(s)?
Chip & Dan Heath - to engineer the switches from fragmented/compartmentalized instruction to whole-task instruction, and from a wide array of content/project management methods to Git & Lab or Hub.
Cal Newport - to build in the practices of deep work among contributors/employees
The living Presidents of the USA - to fund raise, promote the work, recruit contributors, and implement in government training.
## How would selection as a finalist make a difference for your proposed idea(s)?
Publicity

## What types of advice, resources, connections, and/or partnerships would you ideally want to advance your idea(s)?
Chip & Dan Heath - Switch