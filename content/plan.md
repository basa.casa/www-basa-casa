---
title: "Ideas"
draft: false
---

# First Component Institute Plan
Mission - Make the world better by improving human performance, by increasing the adoption of the Four-Component Instructional Design model.
## Idea Validation
1. [ ] Indiegogo campaign pre-launch
- [X] Marketing website - www.basa.casa
  - [X] Convey The rationale for the model - Fragmented vs. Whole-Task Instruction.
  - [X] Contact form
  - [ ] Donate form (stripe)
- [ ] First-component GitHub/GitLab projects
  - [ ] Hugo Theme
  - [ ] Content as submodules

## Finance

1. [ ] Get Customers
1. [ ] Perform
1. [ ] Indiegogo
1. [ ] ICO

## [app.basa.casa](https://gitlab.com/basa.casa/)
Create the seed of the self-hosted product. 

### Platform
- GitLab instance with project templates of static websites for four-component instructional design projects.
- React app with GraphQL backend - relational db CMS

### ID Course
- Instructional Design training
- Online Program Management for Univerisites (ASU EdTech Accelerator)

## Growth & Scale

- The instructional organization, intended to evolve into an University.
- GEO, Horizontal Scalability  
- Become where people go to teach and learn.

Meets Faculty need of purpose, autonomy, and mastery
Is remarkable utility increases naturally
