---
title: "Strategy"
date: "2018-04-04"
lastmod: "2018-04-04"
---
First, a [map](https://medium.com/wardleymaps). Someday I'll narrate.

<img src="https://docs.google.com/drawings/d/e/2PACX-1vSjVlXS1HZwwrva1g36Ljkd-kV-B9F86r3NxStP6fGGy4dzoyZyn8XYwNsvw0N3GqzTYiOr7F5pAe5W/pub?w=960&amp;h=720">

## Four-Component Instructional Design
van Merrienboer's model is stage 3, T1 Educational Technology within the Science-Technology Spiral. It represents the translation of research programs in cognitive psychology onto the generic problem of how to instruct for complex learning. This is an attempt to sell people on the promise of transfer, of improved outcomes for all users of the technology, and to enable them to use the model to design and participate in that instruction. 

## GitLab 
Why not GitHub? Sure. This can be done in any combination of content and project management systems, and we encourage organizations to use what works for them. The important part is that people are using 4C/ID, driven by the true purpose of the education or training problems they apply it to. For us, that technology is GitLab. It is an open-core platform that you can install your own copy of. We will likely exist as our own GitLab server, some day. 

### Git for Content
GitHub changes work in so many ways, and enables distributed content development workflows and integrations make working primarily around Git repositories deliver more - everything, when compared to other CMS/KMS/DMS tools out there. We want to do that for Learning Tasks and other content where needed.

### Integrated Project Management  

van Merrienboer & Kirschner consistently refers to ID projects - these are team-based, long-term, zig-zag designed projects. GitLab provides an excellent interface and set of features ootb. People love GitLab and GitHub for their project management and collaboration features. Communicating about changes, where the change is taking place, changes lives.

### Continuous Delivery 
This final piece of technology is a cultural mindset, combining continuous integration of proposed changes, automatic testing where needed, and push-button deployments and roll-backs. We will teach it, use it, and explore the expansion of its concepts past software delivery, to content, values, and rules.

## Local, Personal, Decentralized

Hugo lets you experience a website without visiting it on a server where you can be tracked. It lets you edit the website, or share its source or output files with someone else, quickly. 

dat:// protocol allows for this type of sharing to be continuous and provides for secret (or public), completely decentralized hosting.

For sites that need public hosting, see our friends at [Netlify](https://www.netlify.com). This site is hosted there!