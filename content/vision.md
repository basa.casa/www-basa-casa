---
title: "My Vision, basacasas Everywhere"
date: 2018-11-21T09:12:24-07:00
draft: true
---
## Dedication
For You, especially K., I., and N.
## Motto
"We Be People. Do Most Good."
# Basa Casa Vision
Basa Casas, or "Task Houses" become how people expect to learn when they enter a new field, new position, or want to adopt a new practice. They are used by every company, school, and government in the eterprise of teaching complex skills. 
## Theoretical Basis 
Generational genetic differences (via random mutation and sexual combinations): DNA :: Working memory : Long-Term Memory
In an organism's lifetime, first its fetus re-plays it's own evolution by natural selection, and then its brain uses a recapitulation of the process of evolution by natural selction in what we call, "learning." This idea, presented by Dr. John Sweller in his 2004, 2006, and subsequent papers, is, to me, the most fundamental and accurate assessment of the phenomenon, how people learn. We accelerate and improve accuracy in overall learning through methods aligned to how we evolved to learn, per hunt.  

Dr. Jeroen J.G. van Merrienboer (2017) makes the same assessment in his chapter, "How People Learn" in chapter 2 of the the Wiley Handbook on Learning. 

It informs the development of the instructional design model we seek to implement widely. 

From to the basic chemical principles underlying the evolution of species and operations of their living organisms, we seek analogues to assist us in the goal of creating a school. We find the technical analogue in networks of git repositories, namely GitHubs, GitLabs, and BitBuckets. The principle of local rules (look this up in Dawkins) by which organisms continuously react to interpretations of stimuli, because the cells they are made of can only do exactly that. Git, Static Site Generators and CMS's provide the rules by which the organs and cells of each site will operate. The rules built into Git-centered Project Mangement Networks are known, logical, more flexible, and shape a path towards diversity, and improvement that cannot be found elsewhere. 

## The Problem

LMS's are generally DB's full of references to uploaded MS Word Documents, the LCMS and the design management systems in place that are most different between implementors. The content in these systems should clearly evolve over time, and benefits from the most collaborative, if not public, environment for its design and development. Its architecture, which should shape the path to designing effective instruction, usually results in default strategies of fragmentation and compartmentalization, instead of more effective whole-task instruction.

## Striving for End Goals by Applying Evidence-Informed Practices

Organizations, teams, and individuals perform complex tasks to produce value for others; clients, patients, students, citizens, patrons, etc.. Maximizing external benefits and minimizing external detriments requires first knowing about relationships between practices and their effects, requiring measurements and research. In the absence of domain-specific research, a hypothesis will suffice until practices demonstrate measurable advantages over those they substitute for. 

Each of the following h3's represent an area where one or more sets of Evidence Informed Practices or Systematic Approaches to Problem Solving must be employed to form a school. The initial organization will implement these strategies as it seeks to create an online school by sharing the open development of . 

### How to Organize People? Corporations, Open-Source Networks, Schools

**Evidence:**
Corporations are pervasive, profit is a proven motive for them. BasaCasa, Inc. is a Delaware Public Benefit Corporation, est. 2018.
BasaCasa operates in public at https://gitlab.com/basa.casa/. 

### What to teach? Domain Practices

**Evidence:** TBD per basacasa.

The things taught with BasaCasa templates should reflect evidence-based (best) practices, usually collected in "handbooks" and the specialized bodies of knowledge and communities publishing information within each discipline. 

### How to teach? Teach with Four-Component Instructional Designs
Ten Steps to Complex Learning: A Systematic Approach to Four-Component Instructional Design improves transfer performance for complex skills by overcoming fragmentation, compartmentalization, and the resulting transfer paradox. This model will be the starting point for all designs hosted on the site. Project and site templates will build this model into their architecture. 

**Evidence:** See [evidence1](https://scholar.google.com/)

basacasa project templates and learning tasks will shape the path to implementing this leading technology in the Instructional Design Profession. The $0 barrier to trying the technologies together, 

### How to manage it? Git-centric Project and Content Management

We recognize GitHub and GitLab as the most intuitive way to manage text shared among many, discuss its implications and any desired changes, and integrating with processes to realize anything possible on a computer network. We find that using the site itself will need a simple basacasa 

**Evidence:** 

### How to Implement it? 

Each site and its replicants have packaging flexibility with git submodules, Publishing Freedom w/.dat, netlify, now.sh, Git, any file host or CDN. This is generally already true, and on top of this stack, we will provide a structured starting point for new sites, and project instructions for their development. Sites may be packaged with a SAP embedded into default Issues and process worksheets built into archetypes and CMS configs. Each piece of content in the site will be a .yml file or one with YAML frontmatter, and associated images. This 

****Evidence:****

## MVP & Example webdev.basa.casa
- Project Initiation
- Interest
- Pre-sale
- Ten Steps Approach 
- Soliciting Contributions
- Rewarding Contributions
- Open the House
- Continuous Delivery
- Marketplace for Tasks and Classes
## Project Templates
`/basacasa/` and `/basacasa-hugo-template/` will enable new 4C/ID's and their teams to create new, succesful projects rapidly. 
## Learning Experience
UI: GitLab/GitHub, VS Code, Netlify, Terminal
## Teaching Experience
UI: Same as students
## Assessment Experience
UI: Forms displayed on locally or remotely running personal basacasa, submitted to graphQL db.
## Financial Model
Pay per professional assessment
Pay for access to content repos
Marketplace model through stripe  
## Equity Distribution
### Methods
#### Contribution-based Equity Grants
#### Simple Agreements for Future Equity 
#### Accredited Investments
### Allocations 
table with rounds/allocation per type: founder, contributor, and investor.

## An Open-Source, Public Benefit, Cooperative Corporation
## competency.basa.casa Sites
## Communications
100% Through GitLab
## Investment
Indiegogo.com, SAFE round, Classic A-IPO, ICO
## Value Streams

## The JFF Billion Dollar Challenge
